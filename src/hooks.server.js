/** @type {import('@sveltejs/kit').Handle} */
export function handle({ event, resolve }) {
	return resolve(event, {
		transformPageChunk: ({ html }) =>
			html.includes('js-enabled$')
				? html.replace('%htmlLang%', 'en').replace('%bodyClasses%', '')
				: html.replace('%htmlLang%', 'en').replace('%bodyClasses%', 'js-enabled')
	});
}
