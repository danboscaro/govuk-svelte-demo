import { parse } from 'yaml';

export async function load(/** @type {any} */ page) {
	const { component } = page.params;
	/** @type {any} */
	const examples = {};
	const yamls = import.meta.glob(`../../../../examples/*.yaml`, { as: 'raw' });
	for (const path in yamls) {
		await yamls[path]().then((f) => {
			const name = path.substring(21, path.indexOf('.yaml'));
			if (name === component) {
				examples[name] = parse(f).examples.filter((/** @type {any} */ e) => !e.hidden);
			}
		});
	}
	return examples;
}
