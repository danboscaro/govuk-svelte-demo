import { parse } from 'yaml';

export async function load({ params }) {
	/** @type {any} */
	const examples = {};
	const yamls = import.meta.glob('../../examples/*.yaml', { as: 'raw' });
	for (const path in yamls) {
		await yamls[path]().then((f) => {
			const name = path.substring(15, path.indexOf('.yaml'));
			examples[name] = parse(f).examples.filter((/** @type {any} */ e) => !e.hidden);
		});
	}
	return {
		examples,
		component: params.component
	};
}
