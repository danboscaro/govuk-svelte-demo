import { componentName } from '$lib';
export async function load({ params }) {
	return {
		title: componentName(params.component)
	};
}
