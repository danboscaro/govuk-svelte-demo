export const componentName = (/** @type {string} */ n) =>
	n.replace(/(^\w|-\w)/g, (n) => n.replace(/-/, '').toUpperCase());
